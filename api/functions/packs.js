// api/functions/packs.js

const Packages = require('../models/packs');

/**
 * Simply gets all packages and returns the array 
 * @param {function(any, Document[])} cb
 */
const getAllPacks = (cb) => {
    Packages.find({}, cb);
}


/** 
 * Returns a package with a specific name if one exists 
 *  
 * @param {String} ns
 * @param {function(any, Document)} cb
 */
const getPackByNamespace = (ns, cb) => {
    Packages.findOne({ namespace: ns }, cb)
}

/**
 * Search by command full name (namespace.command) 
 * 
 * @param {String} name
 * @param {function(any, {index: Number, package: any})} cb
 */
const getCommandByName = (name, cb) => {
    let lastDot;
    for(var i = 0; i < name.length; i++) {
        if(name.charAt(i) == '.') {
            lastDot = i;
        }
    }
    let namespace = name.substring(0, lastDot);
    let commandName = name.substring(lastDot + 1);
    Packages.findOne({ namespace : namespace }, (err, pack) => {
        cb(err, pack, commandName);
    });
}

module.exports = { getAllPacks, getPackByNamespace, getCommandByName };