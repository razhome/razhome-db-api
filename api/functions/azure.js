// api/functions/azure.js
const azure = require('@azure/storage-blob');
const uuid = require('uuid');
const fs = require("fs");
const formidable = require('formidable');
const tmp = require('tmp');
tmp.setGracefulCleanup();

const AzureBlob = require('../models/azure-blob');

var connection = { blobServiceClient: null, containerClient: null };

// Azure Storage functions
/**
 * @returns {{blobServiceClient: azure.BlobServiceClient, containerClient: azure.ContainerClient}}
 */
async function getConnection() {
    if (!connection.blobServiceClient) {
        connection.blobServiceClient = await azure.BlobServiceClient.fromConnectionString(process.env.AZURE_STORAGE_CONNECTION_STRING);
    }
    if (!connection.containerClient) {
        connection.containerClient = await connection.blobServiceClient.getContainerClient("packages");
    }
    return connection;
}

/**
 * 
 * @param {formidable.File} file
 * @returns {Promise<String>} 
 */
async function pushBlobToAzure(file) {
    try {
        let conn = await getConnection();
        const blobName = file.name + '-' + uuid.v1();
        const blockBlobClient = conn.containerClient.getBlockBlobClient(blobName);
        const blobData = fs.readFileSync(file.path);

        await blockBlobClient.upload(blobData.buffer, blobData.byteLength);
        return blobName;
    } catch (err) {
        return Promise.reject(err);
    }
}

/**
 * @returns {Promise<{name:string, createdOn:Date}[]>}
 */
async function listBlobs() {
    try {
        let conn = await getConnection();
        var blobs = [];
        for await (const blob of conn.containerClient.listBlobsFlat()) {
            blobs.push({ name: blob.name, createdOn: blob.properties.createdOn })
        }
        return blobs;
    } catch (err) {
        return Promise.reject(err);
    }
}

/**
 * 
 * @param {string} name 
 * @return {Promise<{tmpFile:tmp.FileResult,filename:string}>}
 */
async function downloadBlob(name) {
    try {
        let conn = await getConnection();
        const blockBlobClient = conn.containerClient.getBlockBlobClient(name);
        const downloadResponse = await blockBlobClient.download(0);
        var tmpFile = tmp.fileSync();
        var fileStream = fs.createWriteStream(tmpFile.name);
        var data = await readStream(downloadResponse.readableStreamBody);
        fs.writeFileSync(tmpFile.name, data);
        return { tmpFile: tmpFile, filename: name.match(/.*rpkg/)[0] };
    } catch (err) {
        return Promise.reject(err);
    }
}

/**
 * 
 * @param {ReadableStream} stream 
 * @returns {Promise<string>}
 */
async function readStream(stream) {
    return new Promise((resolve, reject) => {
        const chunks = [];
        stream.on('data', (data) => {
            chunks.push(data.toString())
        });
        stream.on("end", () => {
            resolve(chunks.join(""));
        });
        stream.on("error", reject);
    })
}

// MongoDB functions
/**
 * Adds a blob to the blob queue
 * 
 * @param {String} name 
 */
async function mongoQueueBlob(name) {
    var newBlob = new AzureBlob();
    newBlob.name = name;
    try {
        var doc = await newBlob.save();
        return doc;
    } catch (err) {
        return Promise.reject(err);
    }

}

/**
 * Gets the most recent blob and deletes it from the database.
 * 
 * @returns {Promise<Document>}
 */
async function mongoDequeueBlob() {
    try {
        var blobs = await AzureBlob.find({}).sort({ added: 1 }).exec();
        if (blobs.length != 0) {
            var dequeuedBlob = { name: blobs[0].name, added: blobs[0].added };
            await AzureBlob.deleteOne({name: blobs[0].name}).exec();
            return dequeuedBlob;
        } else {
            return Promise.reject({ status: 404, err: { message: "No matches found" }});
        }
    } catch (err) {
        return Promise.reject({status: 500, err});
    }


}

/**
 * Gets and returns all blobs from the database
 * 
 * @returns {Promise<Document[]>}
 */
async function mongoGetBlobs() {
    try {
        var blobs = await AzureBlob.find({}).exec();
        return blobs;
    } catch (err) {
        return Promise.reject(err);
    }
}

module.exports = { pushBlobToAzure, listBlobs, downloadBlob, mongoQueueBlob, mongoDequeueBlob, mongoGetBlobs };
