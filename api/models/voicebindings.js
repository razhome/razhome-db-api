// api/model/voicebindings.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const VoiceBindingSchema = new Schema({
    phrase: String,
    namespace: String,
    command: String
});

module.exports = mongoose.models["voiceBinding"] || mongoose.model("voiceBinding", VoiceBindingSchema);
