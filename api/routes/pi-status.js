// api/routes/pi-status.js

const express = require('express');
const router = express.Router();
const PiStatus = require('../models/pi-status');
const statusFunctions = require('../functions/pi-status');

/** Returns the latest status from the raspberry pi */
router.get('/', (req, res) => {
    console.log("Recieved GET on /api/status");
    statusFunctions.getRecentStatus((err, doc) => {
        if(err) {
            res.status(500).send(err);
        } else {
            res.json(doc);
        }
    })
});

/** Updates the status of the raspberry pi
 *  Note: not used in razhome-web */
router.post('/', (req, res) => {
    console.log("Recieved POST on /api/status");
    try {
        if((req.body.status) && (req.body.ip)) {
            var status = new PiStatus({
                status: req.body.status,
                ip: req.body.ip
            });
            status.save().then((doc) => {
                res.send("Updated status to db");
            });
        } else {
            res.send("No body provided")
        }
    } catch(err) {
        res.status(500).send(err);
    }
});

module.exports = router;