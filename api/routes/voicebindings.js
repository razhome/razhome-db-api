// api/routes/voicebindings.js

const router = require('express').Router();
const VoiceModel = require('../models/voicebindings');
const voiceFunctions = require('../functions/voicebindings');

router.get('/', (req, res) => {
    console.log("Recieved GET on /api/voice")
    voiceFunctions.getAllPhrases().then((docs) =>
    {
        console.log(docs);
        res.json(docs);
    });
});

router.post('/', (req, res) => {
    console.log("Recieved POST on /api/voice");
    try {
        var VoiceBind = new VoiceModel({
            phrase : req.body.phrase,
            namespace : req.body.namespace,
            command : req.body.command
        });
        VoiceBind.save();
        res.send("added voicebind to database");
    } catch(err) {
        res.status(500).send(`Unknown error occurred \n${err}`);
    }
});

module.exports = router;