// server.js

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const mongoCreds = require('./creds/mongo.json');
const PORT = 3001;
const logStartup = require('./utils/log-startup');


const db = mongoose.connect("mongodb+srv://" + mongoCreds.username + ":" + mongoCreds.password + mongoCreds.uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: "packages"
});

// allow parsing for POST requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// add api routes
app.use('/api/status', require('./api/routes/pi-status'));
app.use('/api/packs', require('./api/routes/packs.js'));
app.use('/api/voice', require('./api/routes/voicebindings'));
app.use('/api/azure', require('./api/routes/azure'));

app.listen(PORT, (err) => {
    if(err) throw err;
    console.log(`ready on localhost:${PORT}`);
});

logStartup(`http://localhost:${PORT}/api/status`);