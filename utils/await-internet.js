// utils/await-internet.js

const sleep = require('util').promisify(setTimeout);
const dns = require('dns').promises;

async function checkInternet() {
    return dns.lookup('google.com')
        .then(() => true)
        .catch(() => false);
};

async function awaitInternet() {
    hasInternet = await checkInternet()
    while(!hasInternet) {
        await sleep(1000);
        hasInternet = await checkInternet()
    }
}

module.exports = awaitInternet;