// utils/log-startup.js
const axios = require('axios');
const awaitInternet = require('./await-internet');
const publicIp = require('public-ip');
const os = require('os');


async function logStartup(url) {
    if (os.platform() != 'win32') {
        await awaitInternet();
        ip = await publicIp.v4();
        axios.post(url, {
            ip: ip,
            status: "Online"
        });
    }
}

module.exports = logStartup;